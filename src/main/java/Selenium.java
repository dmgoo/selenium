import java.io.File;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import org.openqa.selenium.chrome.ChromeDriver;
/**
 *
 * @author Dawid Karpiński
 */
public class Selenium {

    private static WebDriver driver = new ChromeDriver();
    public static void main(String[] args) {
       
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(1).getSeconds());
        try {
            driver.get("http://www.anaesthetist.com/mnm/javascript/calc.htm");
            assertMultipy();
            getScreenshot();
        } finally {
            driver.quit();
        }
    }
    
    private static void assertMultipy() {
        driver.findElement(By.name("one")).click();
        driver.findElement(By.name("mul")).click();
        driver.findElement(By.name("two")).click();
        driver.findElement(By.name("result")).click();
        if (driver.findElement(By.name("Display")).getAttribute("value").equals("2")) {
            System.out.println("Test Passed");
        } else {
            System.out.println("Test Failed expeded value was 2, but recieved: "
                    + driver.findElement(By.name("Display")).getAttribute("value"));
            throw new AssertionError();
        }
    }

    private static void getScreenshot() {
        try {
            TakesScreenshot scrShot = ((TakesScreenshot) driver);
            File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
            File DestFile = new File("e://screenShot.png");
            FileUtils.copyFile(SrcFile, DestFile);
        } catch (final IOException ex) {
            System.out.println("Failed to take Scvreenshot, "+ ex);
        }
    }
}
